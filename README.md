# SEP Guide - Statistique pour l'Évaluation et la Prévision

Bienvenue dans le guide pour les étudiants en Master de Statistique pour l'Évaluation et la Prévision. 
Ce guide a été créé pour vous aider à réussir dans vos études, en vous fournissant des ressources essentielles, des fiches de révision, des conseils d'installation de logiciels, et des codes généraux pour votre parcours académique. 
Que vous soyez nouvellement inscrits ou que vous cherchiez à approfondir vos connaissances, ce guide devrait vous être précieux.

## Table des matières :
 - Guide M1
 - Guide M2  
 - Programmation 


## Guide M1
Dans cette section, vous trouverez les compétences essentielles que vous devez acquérir et maîtriser avant la rentrée en Master 1. Chaque fiche comprendra une liste de compétences et de rappels utiles pour vous aider à préparer vos examens et à consolider votre compréhension. Il est impératif que vous maîtrisiez ces compétences avant le début de votre programme de Master 1.
Ces fiches de révision vous serviront de référence rapide pour évaluer votre niveau de préparation. Assurez-vous de travailler activement sur ces compétences et de les maîtriser pour réussir dans votre programme académique.
N'hésitez pas à explorer ces fiches pour vous aider à atteindre le niveau de compétence requis pour votre Master 1.

## Guide M2
Dans cette section, vous trouverez les compétences essentielles que vous devez acquérir et maîtriser avant la rentrée en Master 2. Chaque fiche comprendra une liste de compétences et de rappels utiles pour vous aider à préparer vos examens et à consolider votre compréhension. Il est impératif que vous maîtrisiez ces compétences avant le début de votre programme de Master 1.
Ces fiches de révision vous serviront de référence rapide pour évaluer votre niveau de préparation. Assurez-vous de travailler activement sur ces compétences et de les maîtriser pour réussir dans votre programme académique.
N'hésitez pas à explorer ces fiches pour vous aider à atteindre le niveau de compétence requis pour votre Master 2.

## Programmation 
Dans cette section, nous vous fournirons des ressources essentielles pour la programmation en statistique. Vous trouverez des informations sur les logiciels couramment utilisés, des guides d'installation, des explications sur leur utilité, et des exemples de code pour des manipulations générales. La programmation est un outil puissant en statistique. Cette section contient des exemples de codes en Python, R, ou d'autres langages pertinents, qui vous aideront à résoudre des problèmes et à automatiser des tâches courantes dans votre travail statistique. Ces exemples de code peuvent servir de point de départ pour vos propres projets ou analyses.

## Comment Utiliser ce Guide
Pour tirer le meilleur parti de ce guide, suivez ces étapes simples :

Explorez la table des matières ci-dessus pour trouver la section qui correspond à vos besoins.
Ensuite pour chaque section il y a un sommaire spécifique.
Vous avez la posibilité de convertir le guide en PDF ou en HTML en modifiant le code du fichier .qmd correspondant

Bonne chance dans vos études et votre future carrière en statistique !